# json-diff

## Steps:

1. Create virtual environment: virtualenv venv (optional)
2. Activate virtual environment: source venv/bin/activate (optional)
3. Cd to main folder to locate the requirement.txt file and execute: pip install --no-cache-dir -r requirements.txt. 
4. Open rms_json and r2e_json and enter json data
5. run script: python json_diff.py
