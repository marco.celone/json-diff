import json
from deepdiff import DeepDiff

with open('rms_json') as rms, open('r2e_json') as r2e:
  rms_data = json.load(rms)
  r2e_data = json.load(r2e)
  a, b = json.dumps(rms_data, sort_keys=True), json.dumps(r2e_data, sort_keys=True)
  if a == b:
      print("Files are the same")
  else:
      print("Files don't match")
      diff = DeepDiff(rms_data, r2e_data)
      print(json.dumps(diff, indent=4))

